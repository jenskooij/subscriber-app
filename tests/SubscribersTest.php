<?php

namespace App\Tests;

use App\Controller\SubscribersController;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SubscribersTest extends WebTestCase
{
    const TEST_NAME = "Test";
    const SUBSCRIBERS_V1_BASE_URI = '/subscribers/v1/';

    private $lastUniqueEmail;

    private $addedEmails = [];

    /**
     * Test /subscribers to return an array in JSON
     */
    public function testIndex()
    {
        $client = static::createClient();
        $client->request('GET', self::SUBSCRIBERS_V1_BASE_URI);
        $response = $client->getResponse();

        $this->checkJsonAndJsonHeader($response);

        $content = json_decode($response->getContent());
        $this->assertIsArray($content);

        $this->assertResponseIsSuccessful();
    }

    public function testAdd()
    {
        $client = static::createClient();
        $response = $this->addSubscriber($this->getUniqueEmail(), $client);
        $this->addedEmails[] = $this->lastUniqueEmail;

        $content = json_decode($response->getContent());

        $this->assertIsObject($content);
        $this->assertTrue($content->status == SubscribersController::STATUS_SUBSCRIBER_CREATED,
            'Test add subscriber succesful');
        $this->assertResponseIsSuccessful();

        static::$kernel->GetContainer()->get('doctrine')->getManager()->flush();
    }

    public function testShow()
    {
        // Need to add a subscriber before we can verify that we can show one
        $this->testAdd();
        $email = $this->lastUniqueEmail;

        // Check if this subscriber was created
        $uri = self::SUBSCRIBERS_V1_BASE_URI . $email;
        $client = static::$kernel->getContainer()->get('test.client');
        $client->request('GET', $uri);

        $response = $client->getResponse();


        $content = json_decode($response->getContent());
        $this->assertIsObject($content, "Response json from uri " . $uri . ": " . print_r($content, true));

        $this->assertTrue($content->name == self::TEST_NAME, 'Test show subscriber name succesful');
        $this->assertTrue($content->email == $email, 'Test show subscriber email succesful');

        $this->assertResponseIsSuccessful();

    }

    public function testUpdate()
    {
        // Need to add an subscriber before we can verify that we can update one
        $this->testAdd();
        $email = $this->lastUniqueEmail;
        $updatedEmail = str_replace('@', '+updated@', $email);

        // Update the subscriber
        $client = static::$kernel->getContainer()->get('test.client');
        $client->request('PUT', self::SUBSCRIBERS_V1_BASE_URI . $email, [], [], [], "{
            \"name\": \"" . self::TEST_NAME . "\",
            \"email\": \"" . $updatedEmail . "\"
        }");

        // Flush, or the doctrine manager wont save the changes yet.
        static::$kernel->GetContainer()->get('doctrine')->getManager()->flush();

        // Check the updated subscriber exists
        $uri = self::SUBSCRIBERS_V1_BASE_URI . $updatedEmail;
        $client = static::$kernel->getContainer()->get('test.client');
        $client->request('GET', $uri);

        $client->getResponse();
        $this->assertResponseIsSuccessful();

        // Updated the email in array of emails to be deleted
        // Found on https://stackoverflow.com/questions/8668826/search-and-replace-value-in-php-array
        $this->addedEmails = array_replace($this->addedEmails,
            array_fill_keys(
                array_keys($this->addedEmails, $email),
                $updatedEmail
            )
        );
    }

    public function testDelete()
    {
        // Need to add an subscriber before we can verify that we can delete one
        $this->testAdd();
        $email = $this->lastUniqueEmail;

        while (($key = array_search($email, $this->addedEmails)) !== false) {
            unset($this->addedEmails[$key]);
        }

        // Delete the subscriber
        $client = static::$kernel->getContainer()->get('test.client');
        $client->request('DELETE', self::SUBSCRIBERS_V1_BASE_URI . $email);

        $response = $client->getResponse();

        $content = json_decode($response->getContent());
        $this->assertIsObject($content, "Response json: " . print_r($content, true));

        $this->assertTrue($content->status == SubscribersController::STATUS_SUBSCRIBER_DELETED, 'Test delete subscriber succesful');

    }

    protected function tearDown(): void
    {
        foreach ($this->addedEmails as $email) {
            $client = static::$kernel->getContainer()->get('test.client');
            $client->request('DELETE', self::SUBSCRIBERS_V1_BASE_URI . $email);
        }
        parent::tearDown();
    }


    /**
     * Assert Succesful request, which returns json
     * @param Response $response
     */
    protected
    function checkJsonAndJsonHeader(
        Response $response
    ): void {
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
        $this->assertJson($response->getContent());
    }

    /**
     * @param string $subscriberEmail
     * @param KernelBrowser $client
     * @return Response
     */

    protected
    function addSubscriber(
        string $subscriberEmail,
        KernelBrowser $client
    ): Response {
        $client->request('POST', self::SUBSCRIBERS_V1_BASE_URI . 'add', [], [], [], "{
            \"name\": \"" . self::TEST_NAME . "\",
            \"email\": \"" . $subscriberEmail . "\"
        }");
        $response = $client->getResponse();
        $this->checkJsonAndJsonHeader($response);
        return $response;
    }

    /**
     * @return string
     */
    protected
    function getUniqueEmail()
    {
        $this->lastUniqueEmail = md5(microtime()) . '@email.com';
        return $this->lastUniqueEmail;
    }
}
