<?php

namespace App\Controller;

use App\Entity\Subscriber;
use App\Repository\SubscriberRepository;
use App\Service\SubscribersHelper;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use \OpenApi\Annotations as OA;

/**
 * @Route("/subscribers/v1")
 * @OA\Info(title="Subscribers API", version="1.0", description="Simple application for adding, editing, retrieving and deleted subscribers (CRUD)")
 * @OA\Server(url="/subscribers/v1")
 */
class SubscribersController extends AbstractController
{
    const PROP_STATUS = 'status';

    const PARAM_EMAIL = 'email';
    const PARAM_NAME = 'name';
    const STATUS_SUBSCRIBER_CREATED = 'Subscriber created!';
    const STATUS_SUBSCRIBER_DELETED = 'Subscriber deleted!';
    const STATUS_SUBSCRIBER_UPDATED = 'Subscriber updated!';

    /**
     * @Route("/", name="subscribers_index", methods={"GET"})
     * @param SubscriberRepository $subscriberRepository
     * @OA\Get(
     *     path="/",
     *     description="Retrieve all available subscribers",
     *     @OA\Response(
     *      response="200",
     *      description="Overview of subscribers",
     *      @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(ref="#/components/schemas/Subscriber")
     *
     *         ),
     *     )
     *     ),
     *     @OA\Response(
     *      response="default",
     *      description="Unexpected Error",
     *       @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(ref="#/components/schemas/ErrorApiResponse"),
     *       )
     *     )
     * )
     * @return Response
     */
    public function index(SubscriberRepository $subscriberRepository): Response
    {
        return $this->json($subscriberRepository->findAll());
    }

    /**
     * @Route("/add", name="subscribers_add", methods={"POST"})
     * @param Request $request
     * @param SubscribersHelper $subscribersHelper
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     * @OA\Post(
     *     path="/add",
     *     description="Add a new subscriber",
     *     @OA\RequestBody(
     *       @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(ref="#/components/schemas/Subscriber"),
     *      )
     *     ),
     *     @OA\Response(
     *      response="201",
     *      description="Confirmation of subscriber added",
     *      @OA\MediaType(
     *         mediaType="application/json",
     *     )
     *     ),
     *     @OA\Response(
     *      response="default",
     *      description="Unexpected Error",
     *       @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(ref="#/components/schemas/ErrorApiResponse"),
     *       )
     *     )
     * )
     */
    public function add(Request $request, SubscribersHelper $subscribersHelper)
    {
        $subscribersHelper->addSubscriber($request);

        return $this->json([self::PROP_STATUS => self::STATUS_SUBSCRIBER_CREATED], Response::HTTP_CREATED);
    }

    /**
     * @Route("/{email}", name="subscribers_show", methods={"GET"})
     * @param Subscriber $subscriber
     * @OA\Get(
     *     path="/{email}",
     *     description="Retrieve a subscriber by his email",
     *     @OA\Parameter(in="path", name="email",required=true, @OA\Schema(type="string"), description="Email of subscriber"),
     *     @OA\Response(
     *      response="200",
     *      description="The subscriber found by email",
     *       @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(ref="#/components/schemas/Subscriber"),
     *       )
     *     ),
     *     @OA\Response(
     *      response="default",
     *      description="Unexpected Error",
     *       @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(ref="#/components/schemas/ErrorApiResponse"),
     *       )
     *     )
     * )
     * @return Response
     */
    public function show(Subscriber $subscriber): Response
    {
        return $this->json($subscriber);
    }

    /**
     * @Route("/{email}", name="subscribers_update", methods={"PUT"})
     * @param Request $request
     * @param Subscriber $subscriber
     * @param SubscribersHelper $subscribersHelper
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     * @OA\Put(
     *     path="/{email}",
     *     description="Update a subscriber by his email",
     *     @OA\Parameter(in="path", name="email",required=true, @OA\Schema(type="string"), description="Email of subscriber"),
     *     @OA\RequestBody(
     *       @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(ref="#/components/schemas/Subscriber"),
     *      )
     *     ),
     *     @OA\Response(
     *      response="200",
     *      description="Status of the update",
     *      @OA\MediaType(
     *         mediaType="application/json",
     *     )
     *     ),
     *     @OA\Response(
     *      response="default",
     *      description="Unexpected Error",
     *       @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(ref="#/components/schemas/ErrorApiResponse"),
     *       )
     *     )
     * )
     */
    public function update(Request $request, Subscriber $subscriber, SubscribersHelper $subscribersHelper): Response
    {
        $subscribersHelper->updateSubscriber($request, $subscriber);
        return new JsonResponse([self::PROP_STATUS => self::STATUS_SUBSCRIBER_UPDATED]);
    }

    /**
     * @Route("/{email}", name="subscriber_delete", methods={"DELETE"})
     * @param Subscriber $subscriber
     * @param SubscribersHelper $subscribersHelper
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     * @OA\Delete(
     *     path="/{email}",
     *     description="Delete a subscriber by his email",
     *     @OA\Parameter(in="path", name="email",required=true, @OA\Schema(type="string"), description="Email of subscriber"),
     *     @OA\Response(
     *      response="200",
     *      description="Confirmation of subscriber deleted",
     *      @OA\MediaType(
     *         mediaType="application/json",
     *     )
     *     ),
     *     @OA\Response(
     *      response="default",
     *      description="Unexpected Error",
     *       @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(ref="#/components/schemas/ErrorApiResponse"),
     *       )
     *     )
     * )
     */
    public function delete(Subscriber $subscriber, SubscribersHelper $subscribersHelper): Response
    {
        $subscribersHelper->deleteSubscriber($subscriber);
        return new JsonResponse([self::PROP_STATUS => self::STATUS_SUBSCRIBER_DELETED]);
    }
}
