<?php
/**
 * Created by Jens on 10-Feb-20.
 */

namespace App\Service;


use App\Entity\Subscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SubscribersHelper
{
    const PARAM_NAME = 'name';
    const PARAM_EMAIL = 'email';
    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * SubscribersHelper constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Adds a subscriber to the database
     *
     * @param Request $request
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addSubscriber(Request $request)
    {
        $data = $this->getCheckedData($request->getContent());

        $subscriber = new Subscriber();
        $subscriber->setName($data[self::PARAM_NAME]);
        $subscriber->setEmail($data[self::PARAM_EMAIL]);

        $this->entityManager->persist($subscriber);
        $this->entityManager->flush();
    }

    /**
     * Updates a subscriber in the database
     *
     * @param Request $request
     * @param Subscriber $subscriber
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateSubscriber(Request $request, Subscriber $subscriber)
    {
        $data = $this->getCheckedData($request->getContent());

        $subscriber->setName($data[self::PARAM_NAME]);
        $subscriber->setEmail($data[self::PARAM_EMAIL]);

        $this->entityManager->persist($subscriber);
        $this->entityManager->flush();
    }

    /**
     * Deletes a subscriber
     *
     * @param Subscriber $subscriber
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteSubscriber(Subscriber $subscriber)
    {
        $this->entityManager->remove($subscriber);
        $this->entityManager->flush();
    }

    /**
     * json decode
     * check manadatory properties
     * @param $requestContent
     * @return mixed
     */
    protected function getCheckedData($requestContent)
    {
        $data = json_decode($requestContent, true);

        $this->checkEmpty($data[self::PARAM_NAME]);
        $this->checkEmpty($data[self::PARAM_EMAIL]);

        if (!filter_var($data[self::PARAM_EMAIL], FILTER_VALIDATE_EMAIL)) {
            throw new BadRequestHttpException('Mandatory parameter email is not a valid emailaddress!');
        }

        return $data;
    }

    /**
     * @param $field
     */
    protected function checkEmpty($field): void
    {
        if (empty($field)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }
    }
}