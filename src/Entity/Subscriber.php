<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use \OpenApi\Annotations as OA;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubscriberRepository")
 * @OA\Schema()
 */
class Subscriber
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @OA\Property(type="string")
     */
    public $name;

    /**
     * @ORM\Column(type="string", unique=true)
     * @OA\Property(type="string")
     */
    public $email;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }


}
