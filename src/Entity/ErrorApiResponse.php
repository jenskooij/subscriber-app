<?php
/**
 * Created by Jens on 10-Feb-20.
 */

namespace App\Entity;


use Symfony\Component\HttpFoundation\JsonResponse;
use \OpenApi\Annotations as OA;

/**
 * Class ErrorApiResponse
 * @package App\Entity
 * @OA\Schema(
 *
 * )
 */
class ErrorApiResponse extends JsonResponse
{
    /**
     * @OA\Property(type="string")
     */
    protected $message;

    /**
     * @OA\Property(type="integer")
     */
    protected $statusCode;

    public function __construct($message, $statusCode)
    {
        parent::__construct([
            'message' => $message,
            'statusCode' => $statusCode,
        ], $statusCode, [], false);
    }

}